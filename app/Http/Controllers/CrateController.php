<?php

namespace App\Http\Controllers;

use App\Models\Crate;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\DataTables;

class CrateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('crates.index');
    }

    public function getCrates(Request $request)
    {
        if ($request->ajax()) {
            $data = Crate::with('products')->orderBy('id')->get();
            return Datatables::of($data)
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fruits = Product::all()->where('is_fruit');
        $vegetables = Product::all()->where('is_fruit', '=', 0);

        return view('crates.create', compact('fruits', 'vegetables'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        // replace NULL value of discount with 0
        if ($request->input('discount') == null) {
            $request->merge(['discount' => 0]);
        }
        $containsFruit = false;
        $products = Product::all()->whereIn('id', $request->input('products_id'))->sortBy('is_fruit');
        foreach ($products as $product) {
            if ($product->is_fruit) {
                $containsFruit = true;
                break;
            }
        }
        if (!$containsFruit) throw ValidationException::withMessages(['products_id' => 'At least 1 piece of fruit is required']);

        Crate::create($request->all())->products()->sync($request->input('products_id'), false);

        return redirect()->route('crates.index')
            ->with('success', 'Create was created.');
    }
}
