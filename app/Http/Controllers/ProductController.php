<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.index');
    }

    public function getProducts(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::with('supplier')->latest()->get();
            return Datatables::of($data)
                ->addColumn('action', function ($row) {
                    $actionButtons = '<a href="' . route('products.show', $row->id) . '" class="btn btn-secondary btn-sm">Show</a>';
                    $actionButtons .= '<a href="' . route('products.edit', $row->id) . '" class="edit btn btn-success btn-sm">Edit</a>';
                    $actionButtons .= '<form action="' . route('products.destroy', $row->id) . '" method="POST">' . method_field('DELETE') . csrf_field() . '<button class="btn btn-danger btn-sm">Delete</button></form>';
                    return $actionButtons;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = DB::table('suppliers')->get();

        return view('products.create', compact('suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'stock_count' => 'required',
            'price' => 'required',
            'suppliers_id' => 'required',
            'photo' => 'image|mimes:jpeg,jpg,png,gif,svg|max:2048'
        ]);

        if ($request->has('photo')) {
            // save the photo
            $image = $request->file('photo');
            $name = Str::slug($request->input('title')) . '_' . time();
            $filePath = '/image/' . $name . '.' . $image->getClientOriginalExtension();
            $image->storeAs('/image/', $name . '.' . $image->getClientOriginalExtension(), 'public');
        } else {
            $filePath = '/image/noimage.jpg';
        }

        $request->merge(['photo_path' => $filePath]);

        Product::create($request->all());

        return redirect()->route('products.index')
            ->with('success', 'Product created');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $suppliers = DB::table('suppliers')->get();

        return view('products.edit', compact('product', 'suppliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'title' => 'required',
            'stock_count' => 'required',
            'price' => 'required',
            'suppliers_id' => 'required',
            'photo' => 'image|mimes:jpeg,jpg,png,gif,svg|max:2048'
        ]);

        if ($request->has('photo')) {
            // save the new photo and delete the old
            $image = $request->file('photo');
            $name = Str::slug($request->input('title')) . '_' . time();
            $filePath = '/image/' . $name . '.' . $image->getClientOriginalExtension();
            $image->storeAs('/image/', $name . '.' . $image->getClientOriginalExtension(), 'public');
            Storage::disk('public')->delete($product->photo_path);
            $request->merge(['photo_path' => $filePath]);
        }

        $product->update($request->all());

        return redirect()->route('products.index')
            ->with('success', 'Product updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        Storage::disk('public')->delete($product->photo_path);
        $product->delete();

        return redirect()->route('products.index')
            ->with('success', 'Product deleted');
    }
}
