<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crate extends Model
{
    use HasFactory;

    protected $fillable = [
        'discount'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'crate_product', 'crates_id', 'products_id');
    }
}
