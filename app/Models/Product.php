<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    public $timestamps = true;

    protected $casts = [
        'price' => 'float'
    ];

    protected $fillable = [
        'title',
        'stock_count',
        'photo_path',
        'price',
        'suppliers_id',
        'is_fruit'
    ];

    public $sortable = [
        'id',
        'updated_at'
    ];

    public function supplier()
    {
        return $this->hasOne(Supplier::class, 'id', 'suppliers_id');
    }

    public function crates()
    {
        return $this->belongsToMany(Crate::class, 'crate_product', 'products_id', 'crates_id');
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d.m.Y H:i:s');
    }
}
