<?php

use App\Http\Controllers\CrateController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SupplierController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('products.index');
});

Route::get('suppliers/list', [SupplierController::class, 'getSuppliers'])->name('suppliers.list');
Route::get('products/list', [ProductController::class, 'getProducts'])->name('products.list');
Route::get('crates/list', [CrateController::class, 'getCrates'])->name('crates.list');

Route::resource('suppliers', SupplierController::class);
Route::resource('products', ProductController::class);
Route::resource('crates', CrateController::class);
