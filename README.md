Deployment:

git clone https://<user>@bitbucket.org/schmiedm/snadnee-app.git

create DB

run composer install

renaname .env.example: mv .env.example .env

set values in .env accordingly (DB connection, etc)

php artisan key:generate

create DB structure: php artisan migrate

seed data: php artisan db:seed

set permissions:

	chmod -R o+w storage
	
	chmod -R o+w bootstrap/cache
	
configure webserver to snadnee-app/public directory
