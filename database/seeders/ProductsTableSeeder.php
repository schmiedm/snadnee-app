<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();

        $faker = \Faker\Factory::create();

        $suppliersIds = DB::table('suppliers')->pluck('id');
        for ($i = 0; $i < 1000; $i++) {
            Product::create([
                'title' => ucfirst($faker->word()),
                'stock_count' => $faker->numberBetween(0, 100),
                'price' => $faker->randomFloat(2, 0.01, 999.99),
                'photo_path' => null,
                'suppliers_id' => $faker->randomElement($suppliersIds),
                'is_fruit' => rand(0, 1)
            ]);
        }
    }
}
