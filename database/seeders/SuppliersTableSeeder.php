<?php

namespace Database\Seeders;

use App\Models\Supplier;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suppliers')->delete();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Supplier::create([
                'title' => $faker->company
            ]);
        }
    }
}
