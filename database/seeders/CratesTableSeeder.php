<?php

namespace Database\Seeders;

use App\Models\Crate;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CratesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('crates')->delete();

        $faker = \Faker\Factory::create();

        $productsIds = DB::table('products')->pluck('id');
        for ($i = 0; $i < 16; $i++) {
            $crate = Crate::create([
                'discount' => $i % 2 ? $faker->numberBetween(0, 99) : 0
            ]);
            $products = [];
            for ($j = 0; $j < rand(1, 5); $j++) {
                $products[] = array_rand($productsIds->toArray());
            }
            $crate->products()->sync($products);
        }
    }
}
