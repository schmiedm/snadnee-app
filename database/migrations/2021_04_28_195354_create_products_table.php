<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->integer('stock_count')->unsigned();
            $table->string('photo_path')->nullable()->default(null);
            $table->decimal('price')->unsigned();
            $table->foreignId('suppliers_id')->constrained();
            $table->boolean('is_fruit')->default(0); // to track if product is fruit; it would be better to have categories separately
            $table->timestamps();
            $table->index('title');
            $table->index('suppliers_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
