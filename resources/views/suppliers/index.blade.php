@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Suppliers</h2>
            </div>
        </div>
    </div>

    <table class="table table-bordered table-responsive-lg" id="suppliers-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>

    <script type="text/javascript">
        $(function () {
            let table = $('#suppliers-table').DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                ordering: false,
                ajax: {
                    url: "{{ route('suppliers.list') }}"
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'title', name: 'title'}
                ]
            });
        });
    </script>

@endsection
