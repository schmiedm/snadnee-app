@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Crates</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('crates.create') }}" title="Create a crate"> New
                </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-responsive-lg" id="crates-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Products</th>
            <th>Discount (%)</th>
            <th>Price After Discount</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>

    <script type="text/javascript">
        $(function () {
            let table = $('#crates-table').DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                ordering: false,
                ajax: {
                    url: "{{ route('crates.list') }}"
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'products[, ].title', name: 'products'},
                    {data: 'discount', name: 'discount'},
                    {
                        data: function (row) {
                            let sum = 0;
                            for (const val of row.products) sum += val.price;
                            return Math.round((sum - sum * (row.discount / 100)) * 100) / 100;
                        }, name: 'total'
                    },
                ]
            });
        });
    </script>

@endsection
