@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Crate</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('crates.index') }}" title="Back">Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('crates.store') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Discount (%):</strong>
                    <input type="number" class="form-control" name="discount"
                           placeholder="Discount" default="0" min="0" max="100" step="1">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label class="d-block" for="products">Products:</label>
                    <select id="products" name="products_id[]" multiple class="form-control">
                        <optgroup label="Fruit">
                            @foreach($fruits as $fruit)
                                <option value="{{ $fruit->id }}">{{ $fruit->title }}</option>
                            @endforeach
                        </optgroup>
                        <optgroup label="Vegetables">
                            @foreach($vegetables as $vegetable)
                                <option value="{{ $vegetable->id }}">{{ $vegetable->title }}</option>
                            @endforeach
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.16/js/bootstrap-multiselect.min.js"
            integrity="sha512-ljeReA8Eplz6P7m1hwWa+XdPmhawNmo9I0/qyZANCCFvZ845anQE+35TuZl9+velym0TKanM2DXVLxSJLLpQWw=="
            crossorigin="anonymous"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.16/css/bootstrap-multiselect.min.css"
          integrity="sha512-wHTuOcR1pyFeyXVkwg3fhfK46QulKXkLq1kxcEEpjnAPv63B/R49bBqkJHLvoGFq6lvAEKlln2rE1JfIPeQ+iw=="
          crossorigin="anonymous"/>
    <script>
        $(document).ready(function () {
            $('#products').multiselect({
                nonSelectedText: 'Select Products',
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%',
                enableCollapsibleOptGroups: true,
            });
        });
    </script>
@endsection
