@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Products</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('products.create') }}" title="Create a product">New</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="form-group">
        <label>In Stock:</label>
        <label for='stock-filter'></label><select id="stock-filter" class="form-control" style="width: 200px">
            <option value="">--</option>
            <option value="1">Yes</option>
            <option value="0">No</option>
        </select>
    </div>
    <table class="table table-bordered table-responsive-lg" id="products-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Stock Count</th>
            <th>Price</th>
            <th>Photo</th>
            <th>Supplier</th>
            <th>Updated at</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>

    <script type="text/javascript">
        $(function () {
            var table = $('#products-table').DataTable({
                processing: true,
                serverSide: true,
                searchDelay: 350,
                ajax: {
                    url: "{{ route('products.list') }}"
                },
                columns: [
                    {data: 'id', name: 'id', orderable: true, searchable: true},
                    {data: 'title', name: 'title', orderable: false, searchable: true},
                    {data: 'stock_count', name: 'stock_count', orderable: false, searchable: true},
                    {data: 'price', name: 'price', orderable: false, searchable: false},
                    {
                        data: function (row) {
                            let path = '/storage/image/noimage.jpg';
                            if (row.photo_path) path = '/storage' + row.photo_path;
                            return '<img src="' + path + '" width="100px">'
                        }, name: 'photo_path', orderable: false, searchable: false
                    },
                    {data: 'supplier.title', name: 'supplier.title', orderable: false, searchable: true},
                    {data: 'updated_at', name: 'updated_at', orderable: true, searchable: false},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            // in stock filtering
            $('#stock-filter').change(function () {
                if ($(this).val() === '') {
                    table.columns().search('').draw();
                }
                if ($(this).val() === '0') {
                    table.column(2)
                        .search('^[0]$', true)
                        .draw();
                }
                if ($(this).val() === '1') {
                    // stock count has to be greater than 0
                    table.column(2)
                        .search('^[1-9][0-9]*$', true)
                        .draw();
                }
            });
        });
    </script>

@endsection
