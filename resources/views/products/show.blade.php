@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>  {{ $product->title }}</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}" title="Go back">Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Title:</strong>
                {{ $product->title }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Stock count:</strong>
                {{ $product->stock_count }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Photo:</strong>
                <img src="{{ $product->photo_path ? '/storage' . $product->photo_path : '/storage/image/noimage.jpg' }}"
                     width="100px">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Price:</strong>
                {{ $product->price }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Supplier:</strong>
                {{ $product->supplier->title }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Date created:</strong>
                {{ date_format($product->created_at, 'd.m.Y h:m:s') }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Date updated:</strong>
                {{ $product->updated_at }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <strong>Crates with this product:</strong>
            <table class="table table-bordered table-responsive-lg">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Products</th>
                </tr>
                </thead>

                <tbody>
                @foreach($product->crates as $crate)
                    <tr>
                        <td>{{ $crate->id }}</td>
                        <td>@foreach($crate->products as $productInCrate)
                                {{ $productInCrate->title }}{{ ($loop->last ? '' : ',') }}
                            @endforeach
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
